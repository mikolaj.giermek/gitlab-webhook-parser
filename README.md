Gitlab Webhook Parser
=====================

### What is it & Why should I use it?

You want to receive notifications from every project in your Gitlab group.

Gitlab provides integration with Discord notifications, but only on **project level**. On **group level** this integration is not available. Until Gitlab adds automatic Webhook integration with Discord at group level you can use this project. 

It reads JSON received from default Gitlab Webhook, parses it and sent to given URL in proper discord webhook format.

In current version it processing these triggers:
   * Merge request events
   * Wiki page events
   
### How to use

1. Deploy project on your server (e.g. [Heroku](https://www.heroku.com/))
1. Set enviromental variables:
   * `DJANGO_SECRET_KEY` - Set properly
   * `DISCORD_WEBHOOK_URL` - URL to your Discrod webhook
   * `GITLAB_SECRET_TOKEN` - Token used in Gitlab Webhook configuration page
1. Go to `your-project-url.com` to check if project is set up correctly. If you see the welcome message, it should be OK.
1. Go to your group webhook page on Gitlab: `https://gitlab.com/groups/**your-group**/-/hooks`
1. Paste parser URL `your-project-url.com/parser` and `GITLAB_SECRET_TOKEN` in your Gitlab Group webhook page

