class GitLabWebhookParser:
    @staticmethod
    def _wiki_hook(payload):
        ctx = {}
        description = (
            f'{payload["user"]["name"]} ({payload["user"]["username"]})'
            f' has {payload["object_attributes"]["action"]}d a wiki page:'
            f' [{payload["object_attributes"]["title"]}]'
            f'({payload["object_attributes"]["url"]})'
            f' in [{payload["project"]["path_with_namespace"]}]'
            f'({payload["project"]["web_url"]})'
            f'\n'
            f'> {payload["object_attributes"]["message"][:100]}'
        )
        ctx['embeds'] = [
            {
                "author": {
                    "name": payload["user"]["username"],
                    "icon_url": payload["user"]["avatar_url"],
                },
                "description": description,
                "color": 5439232,
            },
        ]
        ctx['content'] = "Wiki alert :notebook_with_decorative_cover:"
        return ctx

    @staticmethod
    def _mr_hook(payload):
        if payload["object_attributes"]["action"] not in ["open", "close"]:
            return {}
        ctx = {}
        description = (
            f'{payload["user"]["name"]} ({payload["user"]["username"]})'
            f' {payload["object_attributes"]["action"]} a merge request'
            f' [!{payload["object_attributes"]["iid"]} {payload["object_attributes"]["title"]}]'
            f'({payload["object_attributes"]["url"]})'
            f' in [{payload["project"]["path_with_namespace"]}]'
            f'({payload["project"]["web_url"]})'
        )
        ctx['embeds'] = [
            {
                "author": {
                    "name": payload["user"]["username"],
                    "icon_url": payload["user"]["avatar_url"],
                },
                "description": description,
                "color": 41983.
            },
        ]
        ctx['content'] = "MR alert :rocket:"
        return ctx
