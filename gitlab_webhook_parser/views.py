from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.exceptions import SuspiciousOperation
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.views.generic import View

from gitlab_webhook_parser.parsers import GitLabWebhookParser as GWP

import requests
import json


class HomeView(TemplateView):
    template_name = 'home.html'


class ParseGitlabWebhookView(View):
    http_method_names = ['post']

    def _get_discord_webhook_url(self):
        url = settings.DISCORD_WEBHOOK_URL
        if not url:
            raise ImproperlyConfigured("You must set 'DISCORD_WEBHOOK_URL' in your settings")
        return url

    def _validate_token(self):
        token = self.request.META.get('HTTP_X_GITLAB_TOKEN', b'')
        if token != settings.GITLAB_SECRET_TOKEN:
            raise SuspiciousOperation("GitLab token is not valid or not included in headers")

    def _select_event(self, payload):
        event_type = self.request.META.get('HTTP_X_GITLAB_EVENT', b'')
        if event_type == "Wiki Page Hook":
            ctx = GWP._wiki_hook(payload)
        elif event_type == "Merge Request Hook":
            ctx = GWP._mr_hook(payload)
        else:
            raise SuspiciousOperation("Event type is not valid or not included in headers")
        return ctx

    def post(self, request, *args, **kwargs):
        self._validate_token()
        body_unicode = request.body.decode('utf-8')
        payload = json.loads(body_unicode)
        ctx = self._select_event(payload)
        try:
            requests.post(
                self._get_discord_webhook_url(),
                data=json.dumps(ctx),
                headers={'Content-type': 'application/json'}
            )
            return HttpResponse('', status=200)
        except Exception as e:
            return HttpResponse('', status=500)
