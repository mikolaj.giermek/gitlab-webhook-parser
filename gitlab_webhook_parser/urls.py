from django.contrib import admin
from django.urls import path

from gitlab_webhook_parser import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.HomeView.as_view(), name='home'),
    path('parser', views.ParseGitlabWebhookView.as_view(), name='parser'),
]
